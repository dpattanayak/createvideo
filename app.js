var app = angular.module("createVideo", ['ngRoute','Login','Dashboard','Createvideo','Signout']);
app.constant('config', {});
app
.factory('httpInterceptor', function ($q, $rootScope, $location, $timeout, $window) {
    return {
        request: function (config) {
            config.headers = config.headers || {};

            if ($rootScope.access_token) {
                config.headers.Authorization = $rootScope.access_token;
                return config;
            } else if ($window.localStorage.token) {
                var tokenvalue = JSON.parse($window.localStorage.getItem("token"));
                config.headers.Authorization = tokenvalue.access_token;
                return config;
            } else {
                return $timeout(function () {
                    config.headers.Authorization = $rootScope.access_token;
                    return config;
                });
            }
            //return config;
        },
        response: function (response) {
            if (response.data !== undefined) {
                if (response.data.status) {
                    if (response.data.message == "session_invalid") {
                        var pat = $location.path()
                        if (pat != "/login") {
                            window.location.href = '/signout';
                        }
                        return $q.reject(response);
                    } else if (response.data.status == "error") {
                        return $q.reject(response);
                    } else {
                        return response || $q.when(response);
                    }
                }
            }
            return response;
        }
    };
})
.config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {

    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/plain';
    $httpProvider.interceptors.push('httpInterceptor');
    $routeProvider
        .when('/login', {
            templateUrl: 'client/login/index.html',
            controller: 'login'
        })
        .when('/dashboard', {
            templateUrl: 'client/dashboard/index.html',
            controller: 'dashboard'
        })
        .when('/createvideo', {
            templateUrl: 'client/createvideo/index.html',
            controller: 'createvideo'
        })
        .when('/signout', {
            templateUrl: 'client/signout/index.html',
            controller: 'signout'
        })
        .otherwise({
            redirectTo: '/login'
        });
        $locationProvider.html5Mode(true);
}])
.directive('ngEnter', function () {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
})
.directive('ngScroll', function () {
    return function (scope, element, attrs) {        
        element.bind('scroll', function (event) {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngScroll);
                });
    
                event.preventDefault();
            }
        })
    };
})
.filter("trustUrl", function ($sce) {
    return function (Url) {
        return $sce.trustAsResourceUrl(Url);
    };
});