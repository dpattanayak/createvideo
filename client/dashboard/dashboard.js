angular.module("Dashboard", []).controller('dashboard', ['$scope', '$http', '$routeParams', '$location', '$window', '$timeout', function ($scope, $http, $routeParams, $location, $window, $timeout) {

    $scope.getdrafvideo = function () {
        $scope.draftvideo = []
        $scope.draftvideo1 = []
        $scope.draftvideo2 = []
        $scope.draftvideo3 = []
        $scope.draftvideo4 = []
        $('#draft').addClass('fa-spin');
        $http.get($scope.baseUrl + "/videos/draft", {})
            .then(function (response) {
                if (response.data.status == "success") {
                    $scope.draftvideo = response.data.data;


                    for (var i = 0; i < $scope.draftvideo.length; i += 4) {
                        $scope.draftvideo1.push($scope.draftvideo[i]);
                    }

                    for (var i = 1; i < $scope.draftvideo.length; i += 4) {
                        $scope.draftvideo2.push($scope.draftvideo[i]);
                    }

                    for (var i = 2; i < $scope.draftvideo.length; i += 4) {
                        $scope.draftvideo3.push($scope.draftvideo[i]);
                    }

                    for (var i = 3; i < $scope.draftvideo.length; i += 4) {
                        $scope.draftvideo4.push($scope.draftvideo[i]);
                    }

                    $scope.isLoading = false;
                } else {
                    console.log("Error");
                }
            })
    }

    $scope.getproducedvideo = function (page, docs) {
        $scope.producedvideo = []
        $scope.producedvideo1 = []
        $scope.producedvideo2 = []
        $scope.producedvideo3 = []
        $scope.producedvideo4 = []
        var url;
        var docsperpage = docs;
        var page = page;
        url = "docsperpage=" + docsperpage + "&page=" + page;
        $('#produce').addClass('fa-spin');

        $http.get($scope.baseUrl + "/videos/produced?" + url, {})
            .then(function (response) {
                if (response.data.status == "success") {
                    $scope.producedvideo = response.data.data;

                    for (var i = 0; i < $scope.producedvideo.length; i += 4) {
                        $scope.producedvideo1.push($scope.producedvideo[i]);
                    }

                    for (var i = 1; i < $scope.producedvideo.length; i += 4) {
                        $scope.producedvideo2.push($scope.producedvideo[i]);
                    }

                    for (var i = 2; i < $scope.producedvideo.length; i += 4) {
                        $scope.producedvideo3.push($scope.producedvideo[i]);
                    }

                    for (var i = 3; i < $scope.producedvideo.length; i += 4) {
                        $scope.producedvideo4.push($scope.producedvideo[i]);
                    }

                    $scope.isLoading = false;
                } else {
                    console.log("Error");
                }
            })
    }

    $scope.getrendereingvideo = function (refress) {

        $scope.rendering = []

        $http.get($scope.baseUrl + "/videos/rendering", {})
            .then(function (response) {
                if (response.data.status == "success") {

                    for (var i = 0; i < response.data.data.length; i++) {
                        if (response.data.data[i].status == 'failed') {
                            response.data.data[i].errorcode = JSON.parse(response.data.data[i].ffmpegerror);
                            console.log(JSON.parse(response.data.data[i].ffmpegerror));
                        }

                    }

                    $scope.rendering = response.data.data;

                } else {
                    console.log("Error");
                }
            })
    }

    $scope.getvideo = function () {
        console.log("Get  Videos.")
        videoid = $routeParams.id;
        $http.get($scope.baseUrl + "/videos?videoid=" + videoid, {})
            .then(function (response) {
                if (response.data.status == "success") {
                    console.log(response, "data video");
                    $scope.videodata = response.data.data;
                } else {
                    alert("Error ")
                }
            })
    }

    $scope.go = function (path, id) {
        url = path + id;
        window.location.href = url;
    }

    $scope.refresh = function () {
        if ($scope.Draft) {
            $scope.getdrafvideo();
        }

        if ($scope.Produced) {
            $scope.getproducedvideo(1, 12);
        }

        if ($scope.Rendering) {
            $scope.getrendereingvideo();
        }
    }

    $scope.goBack = function () {
        window.location.href = '/dashboard';
    }

    $scope.showRendering = function () {

        $('#showRendering').addClass('btn-info');
        $('#showDraft').removeClass('btn-info');
        $('#showProduced').removeClass('btn-info');

        $scope.Rendering = true;
        $scope.Draft = false;
        $scope.Produced = false;

        $scope.getrendereingvideo();
        
    }

    $scope.showDraft = function () {

        $('#showDraft').addClass('btn-info');
        $('#showRendering').removeClass('btn-info');
        $('#showProduced').removeClass('btn-info');

        $scope.Draft = true;
        $scope.Rendering = false;
        $scope.Produced = false;

        $scope.getdrafvideo();
    }

    $scope.showProduced = function () {

        $('#showProduced').addClass('btn-info');
        $('#showDraft').removeClass('btn-info');
        $('#showRendering').removeClass('btn-info');

        $scope.Produced = true;
        $scope.Draft = false;
        $scope.Rendering = false;

        $scope.getproducedvideo(1, 12);
    }

    $scope.init = function () {

        $scope.isLoading = true;
        $scope.downloadVideo = false;
        $scope.Draft = false;
        $scope.Rendering = false;
        $scope.Produced = false;

        $scope.path = $location.path();
        $scope.baseUrl = $window.localStorage.getItem("baseUrl");

        $timeout(function () {
            $scope.refresh();
            $scope.showDraft();
        }, 1000)

        if ($routeParams.id) {
            $scope.downloadVideo = true;
            $scope.getvideo();
        }
    }

    $scope.init();
}]);