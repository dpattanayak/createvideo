angular.module('Signout', []).controller('signout', function($scope, $rootScope, $window, $http, $rootScope, $window) {
        
        $scope.logout = function () {
            console.log("logout")
            $rootScope.user = {
                isLogged: false,
                access_token: '',
                userid: '',
                isLogged: ''
            };
            $window.localStorage.setItem("token", JSON.stringify($rootScope.user));
            $rootScope.checkUser = $rootScope.user.isLogged;
            $rootScope.globalUser = $rootScope.user.userid;
            $rootScope.loginvalues = JSON.parse(localStorage.getItem("token"));
            $http.defaults.headers.common.Authorization = $rootScope.user.access_token;
            window.location.href = "/login";
        }

        $scope.logout();
});
