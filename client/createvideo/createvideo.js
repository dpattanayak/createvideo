angular.module("Createvideo", []).controller('createvideo', ['$scope', '$http', '$routeParams', '$location', '$window', '$timeout', function ($scope, $http, $routeParams, $location, $window, $timeout) {

    $scope.addNew = function () {
        $scope.videodata = {}
        $http.get($scope.baseUrl + "templates/apply?id=5fd85b1b8a6b1914c97ce27e", {})   //old - 5fd0d98e0ed5bf5211ada825
            .then(function (response) {
                if (response.data.status == "success") {
                    var url = '/createvideo?id=' + response.data.id;
                    window.location.href = url;
                    $scope.isLoading = false;
                    $scope.medialibsearch($scope.videodata.video_keyword);
                } else {
                    console.log("Error");
                }
            })
    }

    $scope.getvideodata = function () {
        if ($routeParams.id) {
            videoid = $routeParams.id;
            $http.get($scope.baseUrl + "videos?videoid=" + videoid, {})
                .then(function (response) {
                    if (response.data.status == "success") {
                        $scope.videodata = response.data.data;
                        $scope.isLoading = false;
                        $scope.medialibsearch($scope.videodata.video_keyword);
                    }
                })
        }
    }

    $scope.medialibsearch = function (value) {

        checkboxes = {
            all: true
        }
        $scope.val = value;

        //  Pixabay API
        if (checkboxes.all == true) {
            console.log(value, "checking");
            $.ajax({
                url: "https://pixabay.com/api/?key=7839291-758c68a37914a48440a2a8207&q=" + encodeURIComponent(value) + "&image_type=photo&page=" + $scope.pageNo + "&per_page=" + $scope.per_page,
                dataType: 'json',
                type: 'GET',
                success: function (res) {
                    console.log('RES', res)
                    for (var i = 0; i < res.hits.length; i++) {
                        var a = res.hits[i].imageURL;
                        var b = res.hits[i].previewURL;
                        var imageURLExt = a.split(/\.(?=[^\.]+$)/)[1];
                        var previewURLExt = b.split(/\.(?=[^\.]+$)/)[1];

                        if (imageURLExt != 'jpeg' && previewURLExt != 'jpeg') {
                            $scope.mediadata.push({
                                imageURL: res.hits[i].imageURL,
                                previewURL: res.hits[i].previewURL,
                                source: "pixabay.com",
                                type: "photo",
                                stockid: res.hits[i].id
                            })
                        }
                    }
                    $scope.bar_value = $scope.bar_value + 10;
                    $scope.articleimg = true;
                }
            });
        }

        // Storyblock API

        if (checkboxes.all == true) {
            value = value
            $http.get($scope.baseUrl + "/apicalls?value=" + value + "&page=" + $scope.pageNo + "&per_page=" + $scope.per_page, {}).then(function (response) {
                if (response.data.status == "success") {
                    $scope.userdata = response.data.data;
                    for (var i = 0; i < response.data.data.info.length; i++) {
                        var stockid = response.data.data.info[i].id
                        $scope.mediadata.push({
                            imageURL: response.data.data.info[i].preview_url,
                            previewURL: response.data.data.info[i].preview_url,
                            poster: response.data.data.info[i].thumbnail_url,
                            duration: response.data.data.info[i].duration,
                            title: response.data.data.info[i].title,
                            tags: response.data.data.info[i].keywords,
                            source: "storyblock.com",
                            type: "video",
                            stockid: stockid
                        })
                    }
                    $scope.articleimg = true;
                    $scope.bar_value = $scope.bar_value + 5;
                }
            })
        }

        // Pexel API 

        if (checkboxes.all == true) {
            $scope.pexelkey = "563492ad6f91700001000001f9afd99419a746018a7bca37564e987d";
            $.ajax({
                headers: {
                    "Authorization": $scope.pexelkey
                },
                url: "https://api.pexels.com/v1/search?query=" + value + "&per_page=" + $scope.per_page + "&page=" + $scope.pageNo,
                dataType: 'json',
                type: 'GET',
                success: function (res) {
                    if (res) {
                        for (var i = 0; i < res.photos.length; i++) {
                            var a = res.photos[i].src.original;
                            var b = res.photos[i].src.small;
                            var imageURLExt = a.split(/\.(?=[^\.]+$)/)[1];
                            var previewURLExt = b.split(/\.(?=[^\.]+$)/)[1];

                            if (imageURLExt != 'jpeg' && previewURLExt != 'jpeg') {
                                $scope.mediadata.push({
                                    imageURL: res.photos[i].src.original,
                                    previewURL: res.photos[i].src.small,
                                    source: "pexels.com",
                                    type: "photo"
                                })
                            }
                        }
                    }
                    $scope.articleimg = true;
                }
            });
        }

        $scope.pageNo++;
        setTimeout(function () {
            if ($scope.isLoading) {
                $scope.isLoading = false;
                $scope.search = true;
            }
        }, 1000)
        console.log($scope.mediadata, '<<< mediadata')
    }

    $scope.clearmediaarray = function (value) {
        $scope.mediadata = [];
        $scope.pageNo = 1;
        $scope.per_page = 10;
    }

    $scope.onSearchclick = function (value) {

        $scope.isLoading = true;
        $scope.search = false;

        $scope.clearmediaarray();
        $scope.medialibsearch(value);

    }

    $scope.showSearch = function (data) {
        $scope.activeSlide = data.allslidecount;
        $scope.search = true;
    }

    $scope.goBack = function () {
        $scope.search = false;
    }

    $scope.setBG = function (data) {

        var index = $scope.activeSlide;

        $scope.videodata.slides[index].bg_media.type = data.type;
        $scope.videodata.slides[index].bg_media.source = data.source;
        $scope.videodata.slides[index].bg_media.stockid = data.stockid;
        $scope.videodata.slides[index].bg_media.mediaurl = data.imageURL;
        $scope.videodata.slides[index].bg_media.previewurl = data.previewURL;

        if (data.type == "video") {
            getVideoDimensionsOf(data.imageURL, index, function (i, width, height, videoduration) {

                var videoduration = Math.floor(videoduration);

                $scope.videodata.slides[i].bg_media_width = width;
                $scope.videodata.slides[i].bg_media_height = height;

                $scope.videodata.slides[i].bg_media.video_duration = videoduration;
                $scope.videodata.slides[i].bg_media.slidevideo_duration = videoduration;
                $scope.videodata.slides[i].bg_media.trimEndTime = videoduration;

                if (width < height && $scope.videodata.branding.videotype != 'vertical' && $scope.videodata.branding.videotype != 'square') {
                    $scope.videodata.slides[i].smallimage = true;
                } else {
                    $scope.videodata.slides[i].smallimage = false;
                }
            });

        } else {
            getMeta(data.imageURL, function (width, height) {
                $scope.videodata.slides[index].bg_media_width = width;
                $scope.videodata.slides[index].bg_media_height = height;
                if (width < height && $scope.videodata.branding.videotype != 'vertical' && $scope.videodata.branding.videotype != 'square') {
                    $scope.videodata.slides[index].smallimage = true;
                } else {
                    $scope.videodata.slides[index].smallimage = false;
                }

                if ($scope.videodata.branding.videotype != 'horizontal') {
                    $scope.videodata.slides[index].smallimage = false;
                }

                if ($scope.videodata.slides[index].smallimage == true) {
                    $scope.videodata.slides[index].align_position = "center_left";
                    $scope.videodata.slides[index].img_animation = 'none';
                }

            });
        }

        $scope.search = false;
        console.log("$scope.videodata.slides[$scope.activeSlide] >> ", $scope.videodata.slides[$scope.activeSlide])
        $timeout(function () {
            $scope.$apply()
        }, 3000)
    }

    function getMeta(url, callback) {
        var img = new Image();
        img.onload = function () {
            callback(this.width, this.height);
        };
        img.src = url;
    }

    function getVideoDimensionsOf(url, index, callback) {
        return new Promise(function (resolve) {
            var video = document.createElement('video');
            video.addEventListener("loadedmetadata", function () {
                callback(index, this.videoWidth, this.videoHeight, this.duration);
            }, false);
            video.src = url;
        });
    }

    $scope.addslide = function (index) {
        if ($scope.videodata.slides.length) {
            checknewvideo = true;
            index = index + 1;
            $scope.videodata.slides.splice(index, 0, {
                type: 'slider',
                subtitle: '',
                text: "",
                sub_text: '',
                text_type: "text_slides",
                shadow: true,
                shadowvalue: 0,
                activestatus: true,
                overflow_slide: false,
                customTiming: false,
                fontsize: 1.5,
                bg_media: {
                    type: 'photo',
                    smallimage: false,
                    background_bar: "",
                    mediaurl: $scope.mediadata[index].imageURL,
                    previewurl: $scope.mediadata[index].previewURL,
                    uploade_type: 'api',
                    adjustVolume: true,
                    media_volume: 2,
                    backgroundVolume: 0.2
                },
                branding: {
                    text_color: $scope.videodata.branding.text_color,
                    font: $scope.videodata.branding.font,
                    font_family: $scope.videodata.branding.font_family,
                    shadowtype: 'None',
                    shadow: 'None',
                    text_style: 'None',
                    rgbafill: "#51b4db",
                    rgbabars: "#51b4db",
                    rgbaBar1: '#58c2c899',
                    rgbaBar2: '#00000099',
                    fillbackgroundcolor: '#51b4db',
                    barsbackgroundcolor: '#51b4db',
                    bar1_backgroundcolor: '#58c2c899',
                    bar2_backgroundcolor: '#00000099',
                    backgroundopacity: '100',
                    fillbackgroundopacity: '100',
                    barsbackgroundopacity: '100',
                    backgroundopacityBar1: '0',
                    backgroundopacityBar2: '100',
                    texteffect: 'fade',
                    textimg: './img/textanimation/Fade text animation.mp4',
                    textlabel: 'Fade',
                    textboxeffect: 'None',
                    boxshadowtype: 'None',
                    boxshadow: 'None',
                    text_stroke: 'transparent',
                    transform: 'none',
                    filter: 'none',
                    shadowProp: [{
                        lift: {
                            intensity: "50"
                        },
                        hollow: {
                            thickness: "50"
                        },
                        splice: {
                            thickness: "50",
                            offset: "50",
                            direction: "-45",
                            colour: "#D8D8D8"
                        },
                        echo: {
                            offset: "50",
                            direction: "-45",
                            colour: "#D8D8D8"
                        },
                        glitch: {
                            offset: "50",
                            direction: "90",
                            colour: "Red"
                        },
                        neon: {
                            intensity: "50"
                        }
                    }]
                },
                subtitle_branding: {
                    subText_style: 'None',
                    text_color: $scope.videodata.subtitle_branding.text_color,
                    font: $scope.videodata.subtitle_branding.font,
                    font_family: $scope.videodata.subtitle_branding.font_family,
                    shadowtype: 'None',
                    shadow: 'None',
                    subtitlergba: '#51b4db',
                    subtitlergbafill: '#51b4db',
                    subtitlergbabars: '#51b4db',
                    fillbackgroundcolor: '#51b4db',
                    barsbackgroundcolor: '#51b4db',
                    backgroundopacity: '100',
                    fillbackgroundopacity: '100',
                    barsbackgroundopacity: '100',
                    texteffect: 'fade',
                    textimg: './img/textanimation/Fade text animation.mp4',
                    textlabel: 'Fade',
                    textboxeffect: 'None',
                    boxshadowtype: 'None',
                    boxshadow: 'None',
                    text_stroke: 'transparent',
                    transform: 'none',
                    filter: 'none',
                    shadowProp: [{
                        lift: {
                            intensity: "50"
                        },
                        hollow: {
                            thickness: "50"
                        },
                        splice: {
                            thickness: "50",
                            offset: "50",
                            direction: "-45",
                            colour: "#D8D8D8"
                        },
                        echo: {
                            offset: "50",
                            direction: "-45",
                            colour: "#D8D8D8"
                        },
                        glitch: {
                            offset: "50",
                            direction: "90",
                            colour: "Red"
                        },
                        neon: {
                            intensity: "50"
                        }
                    }]
                },
                align_position: 'center'
            });

            $scope.videodata.slides[index].bg_media.stockid = $scope.mediadata[index].stockid

            $scope.countof();

            $scope.start = index - 2;
            $scope.end = index + 2;
            $scope.lazyup = false;
            $scope.slideloadingtop = false;
            $scope.shoescrollingup = false;
            $scope.shoescrollingdown = false;

            $scope.lazyload();
        }

        $timeout(function () {
            getMeta($scope.mediadata[index].imageURL, function (width, height) {

                var slidedata = $scope.videodata.slides[index];

                slidedata.bg_media_width = width;
                slidedata.bg_media_height = height;
                if (width < height && $scope.videodata.branding.videotype != 'vertical' && $scope.videodata.branding.videotype != 'square') {
                    slidedata.smallimage = true;
                } else {
                    slidedata.smallimage = false;
                }

                if ($scope.videodata.branding.videotype != 'horizontal') {
                    slidedata.smallimage = false;
                }

                if (slidedata.smallimage == true) {
                    slidedata.align_position = "center_left";
                    slidedata.img_animation = 'none';
                }

            });
        }, 1000)

    }

    $scope.deleteslide = function (index) {

        $scope.videodata.slides.splice(index, 1);
        $scope.animation(index);

        if ($scope.videodata.slides[index]) {
            $scope.animation(index);
        } else {
            $scope.animation(index - 1);
        }
    }

    $scope.lazyload = function () {
        for (var i = 0; i < $scope.videodata.slides.length; i++) {
            if (i <= $scope.end && i >= $scope.start) {
                $scope.videodata.slides[i].activestatus = true;
            } else {
                $scope.videodata.slides[i].activestatus = false;
            }
        }
        if ($scope.lazyup == false) {
            var scrolllength = $scope.activeslide;
            $scope.animation(scrolllength);
        }
    }

    $scope.animation = function (len) {
        $timeout(function () {
            if ($('.slideWrap #slideid' + len).position()) {
                var lastElementTop = $('.slideWrap #slideid' + len).position().top;
                var scrollAmount = lastElementTop;
                $('.slideWrap, #slideid' + len).animate({
                    scrollTop: scrollAmount
                }, 1000);
                $timeout(function () {
                    $scope.shoescrollingup = true;
                    $scope.shoescrollingdown = true;
                    $scope.slideloadingtop = true;
                }, 1000);

            }
        })
    }

    $scope.countof = function (change, slideDuration, index, customTiming, operator, key) {
        if ($scope.countofnew) {
            checknewvideo = true;
        } else {
            checknewvideo = false;
        }
        $scope.countofnew = true;
        var videodata = $scope.videodata;
        var vidduration = 0;
        $scope.overflow = false;

        for (var i = 0; i < videodata.slides.length; i++) {

            if ($scope.videodata.slides[i].overflow_slide == false) {
                if (i == videodata.slides.length - 1) {
                    $scope.videodata.slides[i].addbtn = true;
                } else {
                    if ($scope.videodata.slides[i + 1].overflow_slide == true) {
                        $scope.videodata.slides[i].addbtn = false;
                    } else {
                        $scope.videodata.slides[i].addbtn = true;
                    }
                }
            } else {
                if (i == videodata.slides.length - 1) {
                    $scope.videodata.slides[i].addbtn = true;
                } else {
                    if ($scope.videodata.slides[i + 1].overflow_slide == true) {
                        $scope.videodata.slides[i].addbtn = false;
                    } else {
                        $scope.videodata.slides[i].addbtn = true;
                    }
                }
            }

            var slideduration = 0,
                slidewordcount;
            if (videodata.branding.tts) {
                if (!customTiming) {
                    if (!$scope.videodata.customTiming && videodata.slides[i].type == 'slider') {
                        var text = ""
                        if (videodata.slides[i].desc != '') {
                            text = videodata.slides[i].desc.replace(/(<([^>]+)>)/ig, "");
                        }
                        if (videodata.slides[i].text != '') {
                            text = text + " " + videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                        }
                        var s = text ? text.split(/\s+/) : 0; // it splits the text on space/tab/enter
                        var textcount = s.length;
                        slidewordcount = textcount;
                        slideduration = Math.round((slidewordcount * 400) / 1000);


                        if (videodata.slides[i].text_type == "quote_slides" || videodata.branding.text_style == "Double Fill") {
                            var subtextcount;
                            var stex = videodata.slides[i].sub_text.replace(/(<([^>]+)>)/ig, "");
                            var st = stex ? stex.split(/\s+/) : 0; // it splits the text on space/tab/enter
                            subtextcount = st.length;
                            if (subtextcount) {
                                slidewordcount = slidewordcount + subtextcount;
                                slideduration = Math.round((slidewordcount * 400) / 1000);
                            }
                        }

                        if (typeof videodata.slides[i].subtitle != 'undefined' && videodata.slides[i].subtitle != "") {
                            var stex = videodata.slides[i].subtitle.replace(/(<([^>]+)>)/ig, "");
                            var st = stex ? stex.split(/\s+/) : 0; // it splits the text on space/tab/enter
                            subtextcount = st.length;
                            if (subtextcount) {
                                slidewordcount = slidewordcount + subtextcount;
                                slideduration = Math.round((slidewordcount * 475) / 1000);
                            }
                        }

                        if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                            slideduration = 3;
                        }

                        $scope.videodata.slides[i].slide_duration = slideduration;
                    }

                    if (videodata.slides[i].text_type == "no_text_slides" && $scope.videodata.slides[i].type == "slider") {
                        if (videodata.slides[i].bg_media.mediaurl == '' && videodata.slides[i].bg_media.type == 'photo') {
                            $scope.videodata.slides[i].slide_duration = 3
                        } else if (videodata.slides[i].bg_media.mediaurl != '' && videodata.slides[i].bg_media.type == 'photo' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 5
                        } else if (videodata.slides[i].bg_media.mediaurl != '' && videodata.slides[i].bg_media.type == 'photo' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration)
                        } else if (typeof videodata.slides[i].bg_media.video_duration != 'undefined' && videodata.slides[i].bg_media.type == 'video' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = videodata.slides[i].bg_media.video_duration;
                        } else if ($scope.videodata.slides[i].slide_duration < 5 && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 5
                        } else {
                            if (typeof change != 'undefined' && change) {
                                $scope.videodata.slides[i].slide_duration = 5
                            } else {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration)
                            }
                        }
                    } else {
                        if (videodata.slides[i].type == "slider" && videodata.slides[i].text.length == 0 && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 3;
                        } else if ($scope.videodata.slides[i].type != "outro") {
                            slideduration = parseInt($scope.videodata.slides[i].slide_duration);
                            if (slideduration > 0) {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                            } else {
                                var text = videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                                var s = text ? text.split(/\s+/) : 0;
                                var textcount = s.length;
                                slidewordcount = textcount;
                                slideduration = Math.round((slidewordcount * 400) / 1000);
                                if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                                    slideduration = 3;
                                }
                                $scope.videodata.slides[i].slide_duration = slideduration;
                            }
                        }
                    }

                    if ($scope.videodata.slides[i].type == "outro") {
                        if ($scope.videodata.branding.outro_type != 'video') {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                        } else {
                            if (typeof $scope.videodata.branding.outro_duration != 'undefined' && $scope.videodata.branding.outro_duration) {
                                $scope.videodata.slides[i].slide_duration = $scope.videodata.branding.outro_duration;
                                $scope.videodata.slides[i].bg_media.video_duration = $scope.videodata.branding.outro_duration;
                            } else {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                            }
                        }
                        $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                        $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].bg_media.trimStartTime = $scope.videodata.slides[i].slide_start;
                    }

                    if ($scope.videodata.slides[i].type == "showIcon") {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 4;
                        $scope.videodata.slides[i].slide_duration = 4;
                        $scope.videodata.slides[i].bg_media.video_duration = 4;
                        $scope.videodata.slides[i].bg_media.slidevideo_duration = 4;
                        $scope.videodata.slides[i].total_slide_duration = 4;
                    }

                    if ($scope.videodata.slides[i].type == "credit" && $scope.videodata.branding.showIcon) {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 1;
                        $scope.videodata.slides[i].slide_duration = 1;
                        $scope.videodata.slides[i].total_slide_duration = 1;
                    }

                    if ($scope.videodata.slides[i].type == "credit" && $scope.videodata.branding.showIcon == false) {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 5;
                        $scope.videodata.slides[i].slide_duration = 5;
                        $scope.videodata.slides[i].total_slide_duration = 5;
                    }

                    if ($scope.videodata.slides[i].type == 'slider' && $scope.videodata.slides[i].overflow_slide) {

                        var text = videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                        var s = text ? text.split(/\s+/) : 0;
                        var textcount = s.length;
                        slidewordcount = textcount;
                        slideduration = Math.round((slidewordcount * 400) / 1000);

                        if (slideduration < 2) {
                            $scope.videodata.slides[i].slide_duration = 2;
                        } else {
                            $scope.videodata.slides[i].slide_duration = slideduration;
                        }

                        if ($scope.videodata.slides[i - 1].overflow_slide) { // 1 main slide and 2 sublides, here for 3rd slide
                            $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                            $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_start + $scope.videodata.slides[i].slide_duration;
                        } else {
                            $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                            $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i - 1].slide_duration + $scope.videodata.slides[i].slide_duration;
                        }

                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_end - $scope.videodata.slides[i].slide_start;
                        $scope.overflow = true;
                    } else {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_end - $scope.videodata.slides[i].slide_start;
                    }

                } else {
                    if (operator == 'minus') {
                        slideDuration = slideDuration - 1;
                    } else if (operator == 'plus') {
                        slideDuration = slideDuration + 1;
                    }

                    if (typeof operator != 'undefined' && operator) {
                        $scope.videodata.slides[index].customTiming = true;
                        $scope.videodata.customTiming = true;
                    }

                    if (typeof videodata.slides[index] != 'undefined') {
                        if (videodata.slides[index].type == 'slider') {
                            if (videodata.slides[index].bg_media.uploade_type != 'upload') {
                                videodata.slides[index].slide_duration = slideDuration;
                            } else if (videodata.slides[index].bg_media.uploade_type == 'upload') {
                                videodata.slides[index].slide_duration = slideDuration;
                                videodata.slides[index].bg_media.video_duration = slideDuration;
                            } else {
                                videodata.slides[index].slide_duration = slideDuration;
                            }
                        }


                        if ($scope.videodata.slides[index].type == "outro") {
                            if ($scope.videodata.branding.outro_type == 'photo') {
                                $scope.videodata.slides[index].slide_duration = slideDuration;
                            } else {
                                // $scope.videodata.branding.outro_fullscreen = true;
                                $scope.videodata.slides[index].bg_media.type = "video";
                                $scope.videodata.slides[index].slide_duration = slideDuration;
                            }
                            if (!$scope.videodata.slides[index].overflow_slide) {
                                $scope.videodata.slides[index].slide_start = 0;
                            } else {
                                $scope.videodata.slides[index].bg_media.trimStartTime = $scope.videodata.slides[index].slide_start;
                            }
                            $scope.videodata.slides[index].slide_end = videodata.slides[index].slide_duration;
                            $scope.videodata.slides[index].total_slide_duration = $scope.videodata.slides[index].slide_end - $scope.videodata.slides[index].slide_start;
                        } else {
                            if ($scope.videodata.slides[index].overflow_slide == false) {
                                $scope.videodata.slides[index].slide_start = 0;
                                $scope.videodata.slides[index].slide_end = $scope.videodata.slides[index].slide_duration;
                            } else {
                                $scope.videodata.slides[index].slide_start = $scope.videodata.slides[i - 1].slide_end;
                                $scope.videodata.slides[index].slide_end = $scope.videodata.slides[index].slide_start + $scope.videodata.slides[index].slide_duration;
                            }
                        }

                    } else {
                        slideduration = parseInt($scope.videodata.slides[i].slide_duration);
                        if (slideduration > 0) {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                        } else {
                            var text = ""
                            if (videodata.slides[i].desc != '') {
                                text = videodata.slides[i].desc.replace(/(<([^>]+)>)/ig, "");
                            }
                            if (videodata.slides[i].text != '') {
                                text = text + " " + videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                            }
                            var s = text ? text.split(/\s+/) : 0;
                            var textcount = s.length;
                            slidewordcount = textcount;
                            slideduration = Math.round((slidewordcount * 400) / 1000);
                            if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                                slideduration = 3;
                            }
                            $scope.videodata.slides[i].slide_duration = slideduration;
                        }
                    }

                    customTiming = false;
                }
            } else {
                if (!customTiming) {
                    if (!$scope.videodata.customTiming && videodata.slides[i].type == 'slider') {
                        if (videodata.slides[i].text != '') {
                            var text = videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                            var s = text ? text.split(/\s+/) : 0; // it splits the text on space/tab/enter
                            var textcount = s.length;
                            slidewordcount = textcount;
                            slideduration = Math.round((slidewordcount * 400) / 1000);
                        }

                        if (videodata.slides[i].desc_type == "quote_slides" || videodata.branding.text_style == "Double Fill") {
                            var subtextcount;
                            var stex = videodata.slides[i].sub_text.replace(/(<([^>]+)>)/ig, "");
                            var st = stex ? stex.split(/\s+/) : 0; // it splits the text on space/tab/enter
                            subtextcount = st.length;
                            if (subtextcount) {
                                slidewordcount = slidewordcount + subtextcount;
                                slideduration = Math.round((slidewordcount * 400) / 1000);
                            }
                        }

                        if (typeof videodata.slides[i].subtitle != 'undefined' && videodata.slides[i].subtitle != "") {
                            var stex = videodata.slides[i].subtitle.replace(/(<([^>]+)>)/ig, "");
                            var st = stex ? stex.split(/\s+/) : 0; // it splits the text on space/tab/enter
                            subtextcount = st.length;
                            if (subtextcount) {
                                slidewordcount = slidewordcount + subtextcount;
                                slideduration = Math.round((slidewordcount * 475) / 1000);
                            }
                        }

                        if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                            slideduration = 3;
                        }

                        $scope.videodata.slides[i].slide_duration = slideduration;
                    }

                    if (videodata.slides[i].desc_type == "no_text_slides" && $scope.videodata.slides[i].type == "slider") {
                        if (videodata.slides[i].bg_media.mediaurl == '' && videodata.slides[i].bg_media.type == 'photo') {
                            $scope.videodata.slides[i].slide_duration = 3
                        } else if (videodata.slides[i].bg_media.mediaurl != '' && videodata.slides[i].bg_media.type == 'photo' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 5
                        } else if (videodata.slides[i].bg_media.mediaurl != '' && videodata.slides[i].bg_media.type == 'photo' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration)
                        } else if (typeof videodata.slides[i].bg_media.video_duration != 'undefined' && videodata.slides[i].bg_media.type == 'video' && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = videodata.slides[i].bg_media.video_duration;
                        } else if ($scope.videodata.slides[i].slide_duration < 5 && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 5
                        } else {
                            if (typeof change != 'undefined' && change) {
                                $scope.videodata.slides[i].slide_duration = 5
                            } else {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration)
                            }
                        }
                    } else {
                        if (videodata.slides[i].type == "slider" && videodata.slides[i].desc.length == 0 && $scope.videodata.slides[i].customTiming == false) {
                            $scope.videodata.slides[i].slide_duration = 3;
                        } else if ($scope.videodata.slides[i].type != "outro") {
                            slideduration = parseInt($scope.videodata.slides[i].slide_duration);
                            if (slideduration > 0) {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                            } else {
                                var text = videodata.slides[i].desc.replace(/(<([^>]+)>)/ig, "");
                                var s = text ? text.split(/\s+/) : 0;
                                var textcount = s.length;
                                slidewordcount = textcount;
                                slideduration = Math.round((slidewordcount * 400) / 1000);
                                if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                                    slideduration = 3;
                                }
                                $scope.videodata.slides[i].slide_duration = slideduration;
                            }
                        }
                    }

                    if ($scope.videodata.slides[i].type == "outro") {
                        if ($scope.videodata.branding.outro_type != 'video') {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                        } else {
                            if (typeof $scope.videodata.branding.outro_duration != 'undefined' && $scope.videodata.branding.outro_duration) {
                                $scope.videodata.slides[i].slide_duration = $scope.videodata.branding.outro_duration;
                                $scope.videodata.slides[i].bg_media.video_duration = $scope.videodata.branding.outro_duration;
                            } else {
                                $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                            }
                        }
                        $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                        $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].bg_media.trimStartTime = $scope.videodata.slides[i].slide_start;
                    }

                    if ($scope.videodata.slides[i].type == "showIcon") {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 4;
                        $scope.videodata.slides[i].slide_duration = 4;
                        $scope.videodata.slides[i].bg_media.video_duration = 4;
                        $scope.videodata.slides[i].bg_media.slidevideo_duration = 4;
                        $scope.videodata.slides[i].total_slide_duration = 4;
                    }

                    if ($scope.videodata.slides[i].type == "credit" && $scope.videodata.branding.showIcon) {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 1;
                        $scope.videodata.slides[i].slide_duration = 1;
                        $scope.videodata.slides[i].total_slide_duration = 1;
                    }

                    if ($scope.videodata.slides[i].type == "credit" && $scope.videodata.branding.showIcon == false) {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = 5;
                        $scope.videodata.slides[i].slide_duration = 5;
                        $scope.videodata.slides[i].total_slide_duration = 5;
                    }

                    if ($scope.videodata.slides[i].type == 'slider' && $scope.videodata.slides[i].overflow_slide) {

                        var text = videodata.slides[i].text.replace(/(<([^>]+)>)/ig, "");
                        var s = text ? text.split(/\s+/) : 0;
                        var textcount = s.length;
                        slidewordcount = textcount;
                        slideduration = Math.round((slidewordcount * 400) / 1000);

                        if (slideduration < 2) {
                            $scope.videodata.slides[i].slide_duration = 2;
                        } else {
                            $scope.videodata.slides[i].slide_duration = slideduration;
                        }

                        if ($scope.videodata.slides[i - 1].overflow_slide) { // 1 main slide and 2 sublides, here for 3rd slide
                            $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                            $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_start + $scope.videodata.slides[i].slide_duration;
                        } else {
                            $scope.videodata.slides[i].slide_start = $scope.videodata.slides[i - 1].slide_end;
                            $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i - 1].slide_duration + $scope.videodata.slides[i].slide_duration;
                        }

                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_end - $scope.videodata.slides[i].slide_start;
                        $scope.overflow = true;
                    } else {
                        $scope.videodata.slides[i].slide_start = 0;
                        $scope.videodata.slides[i].slide_end = $scope.videodata.slides[i].slide_duration;
                        $scope.videodata.slides[i].total_slide_duration = $scope.videodata.slides[i].slide_end - $scope.videodata.slides[i].slide_start;
                    }

                } else {
                    if (operator == 'minus') {
                        slideDuration = slideDuration - 1;
                    } else if (operator == 'plus') {
                        slideDuration = slideDuration + 1;
                    }

                    if (typeof operator != 'undefined' && operator) {
                        $scope.videodata.slides[index].customTiming = true;
                        $scope.videodata.customTiming = true;
                    }

                    if (typeof videodata.slides[index] != 'undefined') {
                        if (videodata.slides[index].type == 'slider') {
                            if (videodata.slides[index].bg_media.uploade_type != 'upload') {
                                videodata.slides[index].slide_duration = slideDuration;
                            } else if (videodata.slides[index].bg_media.uploade_type == 'upload') {
                                videodata.slides[index].slide_duration = slideDuration;
                                videodata.slides[index].bg_media.video_duration = slideDuration;
                            } else {
                                videodata.slides[index].slide_duration = slideDuration;
                            }
                        }


                        if ($scope.videodata.slides[index].type == "outro") {
                            if ($scope.videodata.branding.outro_type == 'photo') {
                                $scope.videodata.slides[index].slide_duration = slideDuration;
                            } else {
                                // $scope.videodata.branding.outro_fullscreen = true;
                                $scope.videodata.slides[index].bg_media.type = "video";
                                $scope.videodata.slides[index].slide_duration = slideDuration;
                            }
                            if (!$scope.videodata.slides[index].overflow_slide) {
                                $scope.videodata.slides[index].slide_start = 0;
                            } else {
                                $scope.videodata.slides[index].bg_media.trimStartTime = $scope.videodata.slides[index].slide_start;
                            }
                            $scope.videodata.slides[index].slide_end = videodata.slides[index].slide_duration;
                            $scope.videodata.slides[index].total_slide_duration = $scope.videodata.slides[index].slide_end - $scope.videodata.slides[index].slide_start;
                        } else {
                            if ($scope.videodata.slides[index].overflow_slide == false) {
                                $scope.videodata.slides[index].slide_start = 0;
                                $scope.videodata.slides[index].slide_end = $scope.videodata.slides[index].slide_duration;
                            } else {
                                $scope.videodata.slides[index].slide_start = $scope.videodata.slides[i - 1].slide_end;
                                $scope.videodata.slides[index].slide_end = $scope.videodata.slides[index].slide_start + $scope.videodata.slides[index].slide_duration;
                            }
                        }

                    } else {
                        slideduration = parseInt($scope.videodata.slides[i].slide_duration);
                        if (slideduration > 0) {
                            $scope.videodata.slides[i].slide_duration = parseInt($scope.videodata.slides[i].slide_duration);
                        } else {
                            var text = videodata.slides[i].desc.replace(/(<([^>]+)>)/ig, "");
                            var s = text ? text.split(/\s+/) : 0;
                            var textcount = s.length;
                            slidewordcount = textcount;
                            slideduration = Math.round((slidewordcount * 400) / 1000);
                            if (slideduration < 3 && $scope.videodata.slides[i].customTiming == false) {
                                slideduration = 3;
                            }
                            $scope.videodata.slides[i].slide_duration = slideduration;
                        }
                    }

                    customTiming = false;
                }
            }

            vidduration = vidduration + videodata.slides[i].slide_duration;
        }


        $scope.videodata.video_duration = vidduration;
        console.log("Videoduration >>", vidduration)

        var mainslidecount = 1;
        for (var i = 0; i < $scope.videodata.slides.length; i++) {
            if ($scope.videodata.slides[i].overflow_slide == false) {
                $scope.videodata.slides[i].mainslidecount = mainslidecount;
                mainslidecount = mainslidecount + 1;
            }
        }
        var allslidecount = 0;
        for (var i = 0; i < $scope.videodata.slides.length; i++) {
            $scope.videodata.slides[i].allslidecount = allslidecount;
            allslidecount = allslidecount + 1;
        }
    }

    $scope.saveVideo = function () {

        $scope.isSaved = 'Saving';

        $http.post($scope.baseUrl + "/videos/saveVideo", $scope.videodata).then(function (response) {
            $scope.isSaved = 'Saved';
        });
    }

    $scope.$watch('videodata', function (value) {
        if (value) {
            $scope.countof();
            $scope.isSaved = 'Save';
        }
    }, true);

    $scope.getHtml = function (slidedata) {

        var text = slidedata.text;

        if (slidedata.type == 'slider') {
            if (slidedata.text_type == 'title_slides') {
                var html = `
                <html>
                    <body>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                        <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/utils/SplitText.min.js"></script>
                        <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineLite.min.js"></script>
                        <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineMax.min.js"></script>
                        <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenLite.min.js"></script>
                        <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenMax.min.js"></script>
                        <link href="https://fonts.googleapis.com/css?family=Antic+Slab&display=swap" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css?family=Noto Sans&display=swap" rel="stylesheet">
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
                        <style type="text/css">#main_container{ width: 100%;}@font-face{font-family:'Bukhari Script Regular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.ttf') format('truetype');}@font-face{font-family:'Horta';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Horta.otf') format('otf');}@font-face{font-family:'Lovelo Line Bold';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.ttf') format('truetype');}@font-face{font-family:'Modular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=MODULAR-14.otf') format('otf');}@font-face{font-family:'Noot';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Noot.otf') format('otf');}@font-face{font-family:'Nickainley Normal';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.ttf') format('truetype');}@font-face{font-family:'Libre Franklin Black';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=librefranklin-black.ttf') format('truetype');}.char{visibility: hidden}.superShadow{text-shadow:0 1px 0 hsl(174,5%,80%),0 2px 0 hsl(174,5%,75%),0 3px 0 hsl(174,5%,70%),0 4px 0 hsl(174,5%,66%),0 5px 0 hsl(174,5%,64%),0 6px 0 hsl(174,5%,62%),0 7px 0 hsl(174,5%,61%),0 8px 0 hsl(174,5%,60%),0 0 5px rgba(0,0,0,.05),1px 3px rgba(0,0,0,.2),3px 5px rgba(0,0,0,.2),5px 10px rgba(0,0,0,.2),0px 10px rgba(0,0,0,.2),0px 20px rgba(0,0,0,.3)!important;}#p_text0{font-size:555%;font-weight:900;line-height: 120%; letter-spacing: 0.337035px;font-family:Antic Slab;color:#fff;font-weight:bold;font-style:normal;text-decoration:none}#p_text0{text-shadow:none;-webkit-text-stroke : #fff;filter : none;transform : none}#p_text0 > .custom_marker{text-shadow:none;font-family:Antic Slab;color:#0cf!important;-webkit-text-stroke : #fff;filter : none;transform : none}#bgp_text0{font-size:555%;font-weight:900;line-height: 120%; letter-spacing: 0.337035px;font-family:Antic Slab;color:#fff;text-shadow:none;}#background0{background: rgba(81,180,219,1);position: absolute;z-index: -1; transform: translate(-50%,0);left:50%;top: 5%;}.nonQuote_contents{position:relative} .main_slides{position:relative} .sub_slides{position:relative;}.main_slides > div{padding:25px;}#background0{box-shadow:rgba(176, 176, 176, 0.7) 10px 12px;width:fit-content;max-width: 95%;}#backgroundsub0{box-shadow:None;width:fit-content;max-width: 95%;}#background0 > span {visibility : hidden; } #backgroundsub0 > span { visibility : hidden; }.main_slides > div{width:fit-content;max-width:95%;}#main_container{position: fixed;top: 50%;transform: translateY(-50%);text-align: center;}.main_slides > div{margin:auto;}</style>
                        <div id="main_container">
                            <div class="nonQuote_contents">
                                <div class="main_slides"><div id="background0"><span id="bgp_text0">${text}</span></div><div id="bounce0"><span id="p_text0">${text}</span></div></div>
                            </div>
                            <script>var tl,t1,t2,t3;tl=new TimelineLite({delay: 1.5});var tl2 = new TimelineMax();tl.add(TweenLite.from("#bounce0",1,{x:-1000,ease:Power2.easeOut}),0);window.timeline = tl;</script>
                        </div>
                    </body>
                </html>`
            } else if (slidedata.text_type == 'text_slides') {
                var html = `
                    <html>
                        <body> 
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                            <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/utils/SplitText.min.js"></script>
                            <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineLite.min.js"></script>
                            <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineMax.min.js"></script>
                            <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenLite.min.js"></script>
                            <script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenMax.min.js"></script>
                            <link href="https://fonts.googleapis.com/css?family=Noto Sans&display=swap" rel="stylesheet">
                            <link href="https://fonts.googleapis.com/css?family=Noto Sans&display=swap" rel="stylesheet">
                            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
                            <style type="text/css">#main_container{ width: 100%;}@font-face{font-family:'Bukhari Script Regular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.ttf') format('truetype');}@font-face{font-family:'Horta';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Horta.otf') format('otf');}@font-face{font-family:'Lovelo Line Bold';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.ttf') format('truetype');}@font-face{font-family:'Modular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=MODULAR-14.otf') format('otf');}@font-face{font-family:'Noot';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Noot.otf') format('otf');}@font-face{font-family:'Nickainley Normal';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.ttf') format('truetype');}@font-face{font-family:'Libre Franklin Black';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=librefranklin-black.ttf') format('truetype');}.char{visibility: hidden}.superShadow{text-shadow:0 1px 0 hsl(174,5%,80%),0 2px 0 hsl(174,5%,75%),0 3px 0 hsl(174,5%,70%),0 4px 0 hsl(174,5%,66%),0 5px 0 hsl(174,5%,64%),0 6px 0 hsl(174,5%,62%),0 7px 0 hsl(174,5%,61%),0 8px 0 hsl(174,5%,60%),0 0 5px rgba(0,0,0,.05),1px 3px rgba(0,0,0,.2),3px 5px rgba(0,0,0,.2),5px 10px rgba(0,0,0,.2),0px 10px rgba(0,0,0,.2),0px 20px rgba(0,0,0,.3)!important;}#p_text1{font-size:555%;font-weight:900;line-height: 120%; letter-spacing: 0.337035px;font-family:Noto Sans;color:#fff;font-weight:bold;font-style:normal;text-decoration:none}#p_text1{text-shadow:None;-webkit-text-stroke : #fff;filter : none;transform : none}#p_text1 > .custom_marker{text-shadow:None;font-family:Noto Sans;color:#0cf!important;-webkit-text-stroke : #fff;filter : none;transform : none}.custom_markersub{font-family:Noto Sans;color:#0cf!important}#bgp_text1{font-size:555%;font-weight:900;line-height: 120%; letter-spacing: 0.337035px;font-family:Noto Sans;color:#fff;text-shadow:None;}#background1{background: rgba(81,180,219,1);position: absolute;z-index: -1;left: 10px; transform: translate(0, 5%);}.nonQuote_contents{position:relative} .main_slides{position:relative} .sub_slides{position:relative;}.main_slides > div{padding:25px;}#background1{box-shadow:rgba(176, 176, 176, 0.7) 10px 12px;width:fit-content;max-width: 95%;}#backgroundsub1{box-shadow:None;width:fit-content;max-width: 95%;}#background1 > span {visibility : hidden; } #backgroundsub1 > span { visibility : hidden; }.main_slides > div{width:fit-content;max-width:95%;}#main_container{position: fixed;top: 100%;transform: translateY(-100%);text-align: left;}</style>
                            <div id="main_container">
                                <div class="nonQuote_contents">
                                    <div class="main_slides">
                                        <div id="background1">
                                            <span id="bgp_text1">${text}</span>
                                        </div>
                                        <div id="bounce1">
                                        <span id="p_text1">${text}</span>
                                    </div>
                                </div>
                            </div>
                            <script>var tl,t1,t2,t3;tl=new TimelineLite({delay: 1.5});var tl2 = new TimelineMax();tl.add(TweenLite.from("#bounce1",1,{x:-1000,ease:Power2.easeOut}),0);window.timeline = tl;</script></div>
                        </body>
                    </html>
                `
            }

            return html;
        } else if (slidedata.type == 'outro') {

            var html = `
            <html><body><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/utils/SplitText.min.js"></script><script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineLite.min.js"></script><script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TimelineMax.min.js"></script><script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenLite.min.js"></script><script src="https://multimedia5.com/plugins/greensock-js-business-green/src/minified/TweenMax.min.js"></script><link href="https://fonts.googleapis.com/css?family=Noto Sans&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Noto Sans&display=swap" rel="stylesheet"><link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"><style type="text/css">#main_container{ width: 100%;}@font-face{font-family:'Bukhari Script Regular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=BukhariScript-Regular.ttf') format('truetype');}@font-face{font-family:'Horta';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Horta.otf') format('otf');}@font-face{font-family:'Lovelo Line Bold';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Lovelo-LineBold.ttf') format('truetype');}@font-face{font-family:'Modular';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=MODULAR-14.otf') format('otf');}@font-face{font-family:'Noot';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Noot.otf') format('otf');}@font-face{font-family:'Nickainley Normal';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.otf') format('otf');src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=Nickainley-Normal.ttf') format('truetype');}@font-face{font-family:'Libre Franklin Black';src:url('https://sandboxapi.multimedia5.com/api/v1/videos/fonts?fontName=librefranklin-black.ttf') format('truetype');}.char{visibility: hidden}.superShadow{text-shadow:0 1px 0 hsl(174,5%,80%),0 2px 0 hsl(174,5%,75%),0 3px 0 hsl(174,5%,70%),0 4px 0 hsl(174,5%,66%),0 5px 0 hsl(174,5%,64%),0 6px 0 hsl(174,5%,62%),0 7px 0 hsl(174,5%,61%),0 8px 0 hsl(174,5%,60%),0 0 5px rgba(0,0,0,.05),1px 3px rgba(0,0,0,.2),3px 5px rgba(0,0,0,.2),5px 10px rgba(0,0,0,.2),0px 10px rgba(0,0,0,.2),0px 20px rgba(0,0,0,.3)!important;}#main_container{height:100%;background-color:#FFFFFF}#outrocta{text-align: center;color: #555;font-weight: 500;word-wrap: break-word;font-size: 400% !important;font-family:Open Sans, sans-serif;}#outroimage{height: 40%;width:auto;margin:10% auto 5% auto}.main_slides > div{width:fit-content;max-width:95%;}#main_container{position: fixed;top: 50%;transform: translateY(-50%);text-align: center;}.main_slides > div{margin:auto;}</style><div id="main_container"><img id="outroimage" src="https://testmedia-newmultimedia5.s3.amazonaws.com/own_outro%2F7916043637-topbestfinder.jpg"><div id="outrocta">${text}</div><script>var tl;tl=new TimelineMax({repeat:0,delay:.5}),tl.staggerFrom("#outroimage",1.3,{transformOrigin:"top center",autoAlpha:0,ease:Power2.easeInOut,rotationY:180},1).staggerFrom("#outrocta", 1, {ease: Linear.easeOut,autoAlpha: 0,}, "group-2-FadeIn");window.timeline = tl;</script></div></body></html>
            `

            return html;
        }
    }


    $scope.renderVideo = function () {
        $scope.renderLoading = true;
        for (var i = 0; i < $scope.videodata.slides.length; i++) {

            if ($scope.videodata.slides[i].type == 'slider') {

                if ($scope.videodata.slides[i].text_type == 'title_slides') {
                    $scope.videodata.slides[i].align_position = 'center';
                } else if ($scope.videodata.slides[i].text_type == 'text_slides') {
                    $scope.videodata.slides[i].align_position = 'bottom_left';
                }
            }

            if (typeof $scope.videodata.slides[i].bg_media.uploade_type == 'undefined' ||
                $scope.videodata.slides[i].bg_media.uploade_type == "") {
                $scope.videodata.slides[i].bg_media.uploade_type = 'api';
            }

            $scope.videodata.slides[i].renderhtml = $scope.getHtml($scope.videodata.slides[i]);
        }

        $scope.videodata.title = $scope.videodata.slides[0].text

        $timeout(function () {
            $http.put($scope.baseUrl + "/videos/updateVideo", $scope.videodata).then(function (response) {
                if (response.data.status == "success") {
                    $scope.renderLoading = false;
                    window.location.href = '/dashboard';
                } else {
                    $scope.renderLoading = false;
                    $scope.invalid_Data = response.data;
                    $(".slideWrap").animate({
                        scrollTop: $('.slideWrap').prop("scrollHeight")
                    }, 1000);
                }
            })
        }, 2000)
    }

    $scope.go = function (path, id) {
        url = path + id;
        window.location.href = url;
    }

    $scope.init = function () {
        $scope.path = $location.path();
        $scope.baseUrl = $window.localStorage.getItem("baseUrl");
        $scope.isLoading = true;
        $scope.isSaved = 'Save';
        $scope.search = false;
        $scope.mediadata = [];
        $scope.pageNo = 1;
        $scope.per_page = 10;

        if ($routeParams.id) {
            $scope.getvideodata();
        } else {
            $scope.addNew();
        }
    }

    $scope.init();
}]);