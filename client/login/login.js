angular.module("Login",[]).controller('login', function ($scope, $http, $rootScope, $window, $location) {

    $scope.login = function () {
        $scope.loginStatus = 'Logging In';
        $http.post($scope.baseUrl + "auth/signin", $scope.user)
            .then(function (response) {
                console.log("response", response);
                response = response.data;
                if (response.retcode == 0) {
                    $rootScope.access_token = response.token;
                    var limit = {
                        access_token: response.token,
                        isLogged: true,
                        firstvideo_flag: response.firstvideo_flag,
                        userid: response.userid,
                        user: response
                    }

                    $rootScope.globalvalue = limit;

                    $rootScope.globalUser = response.userid;
                    $rootScope.loggeduser = limit.user;
                    $window.localStorage.setItem('token', JSON.stringify($rootScope.globalvalue));

                    setTimeout(function () {
                        window.location.href = '/dashboard';
                    }, 1000)
                } else {
                    $scope.err_msg = response.message;
                }
            })
    }

    $scope.init = function () {
        $scope.user = {
            email: '',
            password : ''
        }

        $scope.loginStatus = 'Log In';
        $scope.baseUrl = 'https://sandboxapi.multimedia5.com/api/v1/';
        $window.localStorage.setItem('baseUrl', $scope.baseUrl);
        if($window.localStorage.getItem("token") != null) {
        $rootScope.globalvalue = JSON.parse($window.localStorage.getItem("token"));
            if ($rootScope.globalvalue.isLogged) {                
                window.location.href = '/dashboard';
            } 
        } else {
        }
    }

    $scope.init();
});